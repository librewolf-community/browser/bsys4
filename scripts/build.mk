.PHONY : all clean veryclean build shell

version_file=../../version
version:=$(shell cat $(version_file))
release_file=../../release
release:=$(shell cat $(release_file))

tag=librewolf/bsys4-buildenv-$(distro)


all : build

clean :
	sudo rm -rf work

veryclean : clean
	rm -f typescript



work : 
	mkdir work
	(cd work && tar xf ../../../source/librewolf-$(version)-$(shell cat ../../source/source_release).source.tar.gz)


build : work
	docker run --rm -v $(shell pwd)/work:/work:rw $(tag) sh -c "MACH_USE_SYSTEM_PYTHON=1 /work/librewolf-$(version)-$(release)/mach build"
	docker run --rm -v $(shell pwd)/work:/work:rw $(tag) sh -c "MACH_USE_SYSTEM_PYTHON=1 /work/librewolf-$(version)-$(release)/mach package"
	cp -v work/librewolf-$(version)-$(release)/obj-*/dist/librewolf-$(version)-$(release).en-US.linux-x86_64.tar.bz2 ../../artifacts/$(distro)
	sudo rm -rf work # get rid of it


shell : work
	-docker run -it --rm -v $(shell pwd)/work:/work:rw $(tag) bash

shell-no-work :
	-docker run -it --rm $(tag) bash



# this below is currently not used, as the gitlab CI one hour build time is exceeded.
# source: mozilla-unified/python/mozboot/mozboot/debian.py
install-debian11 install-ubuntu20 install-ubuntu21 install-mint20 :
	apt-get install -y build-essential libpython3-dev m4 unzip uuid zip libasound2-dev libcurl4-openssl-dev libdbus-1-dev libdbus-glib-1-dev libdrm-dev libgtk-3-dev libpulse-dev libx11-xcb-dev libxt-dev xvfb

install-fedora34 install-fedora35 :
	@echo "target install-$(distro) does not yet add new packages."

ci :
	make install-$(distro)
	mv -v ../../source/librewolf-$(version)-$(shell cat ../../source/source_release).source.tar.gz /work
	( cd /work && tar xf librewolf-$(version)-$(shell cat ../../source/source_release).source.tar.gz )
	cp -v ../../assets/build-librewolf.py /work/librewolf-$(version)-$(release)
	( cd /work/librewolf-$(version)-$(release) && python3 /work/librewolf-$(version)-$(release)/build-librewolf.py $(version) $(release) )
	cp -v /work/librewolf-$(version)-$(release)/obj-*/dist/librewolf-$(version)-$(release).en-US.linux-x86_64.tar.bz2 ../../artifacts/$(distro)

