## Prerequisites

* You'll need *Docker*
* It takes a lot of resources to build Firefox, and thus LibreWolf. 8GB ram is a minimum.

## Building the distribution files:

Building should be easy after the repo has been checked out:
```
make source
make all
make pub
```
This will pull docker images from the Docker Hub.

The produced packages will be in the `pub/` subdirectory.

## Building the docker images:

To build the docker images themselves, use:
```
make buildenv
make push
```

# Platforms

## Production:

* debian11: [docker-build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/buildenv/debian11), [build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/build/debian11), [artifact stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/artifacts/debian11).
* ubuntu20: [docker-build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/buildenv/ubuntu20), [build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/build/ubuntu20), [artifact stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/artifacts/ubuntu20).
* mint20: [docker-build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/buildenv/mint20), [build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/build/mint20), [artifact stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/artifacts/mint20).
* fedora34: [docker-build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/buildenv/fedora34), [build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/build/fedora34), [artifact stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/artifacts/fedora34).
* fedora35: [docker-build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/buildenv/fedora35), [build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/build/fedora35), [artifact stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/artifacts/fedora35).

## Experimental:

* macos: [docker-build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/buildenv/macos), [build stage](https://gitlab.com/librewolf-community/browser/bsys4/-/tree/main/build/macos), _artifact stage_.
* windows: ([ticket #142](https://gitlab.com/librewolf-community/browser/windows/-/issues/142) - Compilation machine failed)
